package com.orpheus;

import java.util.Collection;

public class LineItemFolder {
    private String name;
    private Collection<LineItem> lineItems;

    public LineItemFolder(String name, Collection<LineItem> lineItems) {
        this.name = name;
        this.lineItems = lineItems;
    }

    public String getName() {
        return name;
    }

    public Collection<LineItem> getLineItems() {
        return lineItems;
    }
}
