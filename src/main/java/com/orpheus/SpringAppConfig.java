package com.orpheus;

import com.vaadin.spring.annotation.EnableVaadinNavigation;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.EnableVaadin;

@Configuration
@EnableVaadin
@EnableVaadinNavigation
@ComponentScan
public class SpringAppConfig {
}
