package com.orpheus;

public class LineItem {
    private String id;
    private String supplier;
    private double spend;

    public LineItem(String id, String supplier, double spend) {
        this.id = id;
        this.supplier = supplier;
        this.spend = spend;
    }

    public String getId() {
        return id;
    }

    public String getSupplier() {
        return supplier;
    }

    public double getSpend() {
        return spend;
    }
}
