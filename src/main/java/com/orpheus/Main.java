package com.orpheus;

import javax.servlet.ServletException;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.loader.WebappLoader;
import org.apache.catalina.startup.Tomcat;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class Main {

	public static void main(String[] args) throws ServletException, LifecycleException {
		Tomcat tomcat = new Tomcat();
		Context context = tomcat.addContext("", System.getProperty("java.io.tmpdir"));
		Tomcat.addServlet(context, "vaadin", new MyUI.MyUIServlet());
		context.addServletMappingDecoded("/*", "vaadin");
		context.addApplicationListener(ContextLoaderListener.class.getName());
		context.setLoader(new WebappLoader(Thread.currentThread().getContextClassLoader()));
		context.addParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
		context.addParameter("contextConfigLocation", SpringAppConfig.class.getName());
		tomcat.start();
		tomcat.getServer().await();
	}
}
