package com.orpheus;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(name = "grid")
public class GridView extends VerticalLayout implements View {

	private ListDataProvider<LineItem> lineItemListDataProvider;

	@Autowired
	private DataSource dataSource;

	@PostConstruct
    public void init() {
		lineItemListDataProvider = DataProvider.ofCollection(dataSource.getLineItems());
    }

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		HorizontalLayout hLayout = new HorizontalLayout();

		Grid<LineItemFolder> folderList = new Grid<>();
		folderList.addColumn(LineItemFolder::getName).setCaption("Name");
		Collection<LineItem> lineItems = dataSource.getLineItems();
		Collection<LineItemFolder> folders = createFolders(lineItems);
		ListDataProvider<LineItemFolder> folderProvider = DataProvider.ofCollection(folders);
		folderList.setDataProvider(folderProvider);

		VerticalLayout vLayout = new VerticalLayout();
		Grid<LineItem> grid = new Grid<>();
		grid.addColumn(LineItem::getId).setCaption("ID");
		grid.addColumn(LineItem::getSupplier).setCaption("Supplier");
		grid.addColumn(LineItem::getSpend).setCaption("Spend");
		grid.setDataProvider(lineItemListDataProvider);

		TextField filterTextField = new TextField("Filter by supplier");
		filterTextField.setPlaceholder("supplier filter");
		filterTextField.addValueChangeListener(e -> lineItemListDataProvider.setFilter(LineItem::getSupplier,
				supplier -> supplier.contains(e.getValue())));
		filterTextField.setValueChangeMode(ValueChangeMode.EAGER);

		folderList.addItemClickListener(
				i -> {
					lineItemListDataProvider = DataProvider.ofCollection(i.getItem().getLineItems());
					grid.setDataProvider(lineItemListDataProvider);
				});

		grid.setSizeFull();
		vLayout.addComponents(filterTextField, grid);
		hLayout.addComponents(folderList, vLayout);
		this.addComponent(hLayout);
	}

	private Collection<LineItemFolder> createFolders(Collection<LineItem> lineItems) {
		Map<String, List<LineItem>> folders = lineItems.stream()
				.collect(Collectors.groupingBy(l -> l.getSupplier().substring(0, 1), Collectors.toList()));
		return folders.entrySet().stream().map(e -> new LineItemFolder(e.getKey(), e.getValue()))
				.collect(Collectors.toSet());
	}

}
