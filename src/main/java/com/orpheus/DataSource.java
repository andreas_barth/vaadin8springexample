package com.orpheus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.stereotype.Repository;

@Repository
public class DataSource {

	Collection<LineItem> lineItems;

	public DataSource() {
		lineItems = createLineItems();
	}

	public Collection<LineItem> getLineItems() {
		return lineItems;
	}

	private Collection<LineItem> createLineItems() {
		List<LineItem> result = new ArrayList<>();
		for (int i = 0; i < 1_000_000; ++i) {
			result.add(new LineItem("id" + i, RandomStringUtils.random(10, true, false),
					RandomUtils.nextDouble() * 1_000_000));
		}
		return result;
	}
}
